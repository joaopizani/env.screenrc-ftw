screenrc-ftw
============

The screenrc of the winners. A powerful and very simple to use configuration for GNU Screen.
Sit back and enjoy working on your terminal...

Installation is so simple that it does not deserve the name "installation". Namely:

  1. Checkout this repository, and enter it: "cd screenrc-ftw"
  2. Execute the included little script: "./install.sh"

This will make your .screenrc in the home directory point to (source) the file inside this repo,
and then the "installation" is DONE. In case there's an existing .screenrc, do not fear, a
backup will be made under .screenrc.bkp


Niceties
========

Here are all the goodies provided by this config. They are roughly ordered in the order that
I think they are important:

General goodies
---------------

  * Status line with date/time, hostname, session name, the list of windows and highlighting the current window
  * Support for 256 colors, allowing all of your favorite colorful programs to run - worry-free
  * No annoying startup message, no annoying "trashy" text when exiting full-screen programs
  * You CAN use the mouse to switch windows and regions - IF you have it and like it, why not? :)

  * Some comfortable general keybindings:
    + Escape combo mapped to `C-\` (backslash), thus leaving **free** Ctrl-A for **readline's go-to-beggining**.
    + Close Screen completely: `C-/` (Control + slash)
    + Enter copy mode: `C-F3`
    + Paste: `C-F4`


Lots of useful single-keystroke navigation shortcuts
----------------------------------------------------

  * Show previous/next window:  `C-F{1,2}`
  * Jump directly to one of windows 1..4:  `C-F{5..8}`
  * Decrease/increase current region size (either height or width depending on wheter hor./ver. split):  `C-F{9,10}`
  * Focus back/forth to/from the previously focused region:  `C-F11`
  * Cycle between defined layouts:  `C-F12`
  * Show back/forth previously shown window:  `C-\\` (escape-escape)

  * If you want the navigation command to be **global** (sent also to remote displays), then press Shift together with Ctrl.
    + For example:
      - Previous/next window: `C-S-F{1,2}`
      - Go directly to one of the 4 predefined windows: `C-S-F{5,6,7,8}`
      - Previous/next layout: `C-S-F{9,10}`


Nice predefined windows and region layouts
------------------------------------------

  * Predefined layouts, among which you can easily alternate:
    + One shell, occupying the whole terminal (default when Screen starts)
    + Two shells, horizontally
    + Two regions, vertically

