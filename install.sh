#!/usr/bin/env bash

DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd)"

sudo apt install screen

SCREENRC_TRG_DEFAULT="${HOME}/.screenrc"
SCREENRC_TRG="${1:-"${SCREENRC_TRG_DEFAULT}"}"

NAVBINDS_MUXRC_TRG_DEFAULT="${HOME}/.screenrc-navbindings-mux"
NAVBINDS_MUXRC_TRG_RAW="${2:-"${NAVBINDS_MUXRC_TRG_DEFAULT}"}"
NAVBINDS_MUXRC_TRG="${NAVBINDS_MUXRC_TRG_RAW/#$HOME/\$HOME}"

SCREENRC_SRC_RAW="${DIR}/screenrc"
SCREENRC_SRC="${SCREENRC_SRC_RAW/#$HOME/\$HOME}"


[ -e "${SCREENRC_TRG}" ] && [ ! -L "${SCREENRC_TRG}" ] && mv "${SCREENRC_TRG}" "${SCREENRC_TRG}.bkp"
echo "source \"${SCREENRC_SRC}\"" > "${SCREENRC_TRG}"


"${DIR}/navbindings-options/navbindings-select.sh" "${NAVBINDS_MUXRC_TRG_RAW}"
echo "source \"${NAVBINDS_MUXRC_TRG}\"" >> "${SCREENRC_TRG}"

