#!/usr/bin/env bash

DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd)"

MUXRC_TRG_DEFAULT="${HOME}/.screenrc-navbindings-mux"
MUXRC_TRG="${1:-"${MUXRC_TRG_DEFAULT}"}"

RCFILESDIR_DEFAULT_="${DIR}/rcfiles"
RCFILESDIR_DEFAULT="$(readlink -m "${RCFILESDIR_DEFAULT_}")"
RCFILESDIR_="${2:-"${RCFILESDIR_DEFAULT_}"}"
RCFILESDIR="$(readlink -m "${RCFILESDIR_}")"


nav-options-list() {
    echo "Possibilities of terminals to be chosen from:"

    CNT=0
    NAVOPTIONS_ARRAY=( "${RCFILESDIR}"/* )
    for opt in "${NAVOPTIONS_ARRAY[@]}"; do
        echo "${CNT}) $(basename "${opt}")"
        (( CNT++ ))
    done
}

nav-options-select() {
    nav-options-list
    read -e -n 1 -p 'Desired target terminal config > ' OPTCODE
    NAVOPTIONS_ARRAY_CHOSEN_RAW="${NAVOPTIONS_ARRAY[${OPTCODE}]}"
    NAVOPTIONS_ARRAY_CHOSEN="${NAVOPTIONS_ARRAY_CHOSEN_RAW/#$HOME/\$HOME}"
    echo "source \"${NAVOPTIONS_ARRAY_CHOSEN}\"" > "${MUXRC_TRG}"
}


nav-options-select

