SAS_STATE_ROOT="${XDG_RUNTIME_DIR:-"/run/user/$(id -u)"}/sas-ssh-agent-screen"


function __sas_term_up {
  local SAS_STATE="${SAS_STATE_ROOT}/${2}"
  local SAS_SSHAUTHSOCK_ORIG_WIN="${SAS_STATE}/orig_SSHAUTHSOCK_win-${3:-"${WINDOW}"}"

  if [[ -n "${SSH_AUTH_SOCK}" ]]; then   # Save current value into window-specific file
    mkdir -p "${SAS_STATE}"
    ln -snf "${SSH_AUTH_SOCK}" "${SAS_SSHAUTHSOCK_ORIG_WIN}"
  fi

  if [[ -n "${1}" ]]; then  export SSH_AUTH_SOCK="${1}";  fi
}

function __sas_term_down {
  local SAS_STATE="${SAS_STATE_ROOT}/${1}"
  local SAS_SSHAUTHSOCK_ORIG_WIN="${SAS_STATE}/orig_SSHAUTHSOCK_win-${2:-"${WINDOW}"}"

  if [[ -L "${SAS_SSHAUTHSOCK_ORIG_WIN}" ]]; then   # Restore from window-specific file into variable
    export SSH_AUTH_SOCK="$(readlink "${SAS_SSHAUTHSOCK_ORIG_WIN}")"
    rm -f "${SAS_SSHAUTHSOCK_ORIG_WIN}"
  else
    unset SSH_AUTH_SOCK
  fi

  rmdir "${SAS_STATE}" &>/dev/null;  rmdir "${SAS_STATE_ROOT}" &>/dev/null  # sure cleanup
}


function __sas_up {
  local SAS_SNAME="${1:-"none"}"
  local SAS_STATE="${SAS_STATE_ROOT}/${SAS_SNAME}"
  local SAS_SSHAUTHSOCK_ORIG_SESS_LINK="${SAS_STATE}/orig_SSHAUTHSOCK_session"

  # Don't do anything without a valid SSH_AUTH_SOCK value and an existing matching session
  if [ -n "${SSH_AUTH_SOCK}" ]  &&  screen -ls "${SAS_SNAME}" &>/dev/null; then
    # Save current value into screen-session-specific file, if there is a current value
    SAS_SSHAUTHSOCK_ORIG_SESS_TRG="$(screen -S "${SAS_SNAME}" -Q echo '$SSH_AUTH_SOCK')"
    if [[ -n "${SAS_SSHAUTHSOCK_ORIG_SESS_TRG}" ]]; then
      mkdir -p "${SAS_STATE}"
      ln -sfn "${SAS_SSHAUTHSOCK_ORIG_SESS_TRG}" "${SAS_SSHAUTHSOCK_ORIG_SESS_LINK}"
    fi

    # Update the var in the shell of each window (stopping/backgrounding/resuming blocking process if needed)
    screen -S "${SAS_SNAME}"  -X at '#' stuff  '^Z';                                                                     sleep 0.4
    screen -S "${SAS_SNAME}"  -X at '#' stuff  "__sas_term_up \"${SSH_AUTH_SOCK}\" \"${SAS_SNAME}\"; fg &>/dev/null^M";  sleep 0.4

    screen -S "${SAS_SNAME}"  -X setenv SSH_AUTH_SOCK "${SSH_AUTH_SOCK}"   # Update var in screen env (new windows)
  fi
}

function __sas_down {
  local SAS_SNAME="${1:-"none"}"
  local SAS_STATE="${SAS_STATE_ROOT}/${SAS_SNAME}"
  local SAS_SSHAUTHSOCK_ORIG_SESS_LINK="${SAS_STATE}/orig_SSHAUTHSOCK_session"

  if screen -ls "${SAS_SNAME}" &>/dev/null; then   # Restore the state of the windows (maybe no session if exited)
    screen -S "${SAS_SNAME}" -X at '#' stuff '^Z'  2>/dev/null;                                                  sleep 0.4
    screen -S "${SAS_SNAME}" -X at '#' stuff "__sas_term_down \"${SAS_SNAME}\"; fg &>/dev/null^M"  2>/dev/null;  sleep 0.4

    # Restore the state of the var in screen session itself (if it is saved)
    if [[ -L "${SAS_SSHAUTHSOCK_ORIG_SESS_LINK}" ]]; then
      screen -S "${SAS_SNAME}" -X setenv SSH_AUTH_SOCK "$(readlink "${SAS_SSHAUTHSOCK_ORIG_SESS_LINK}")" 2>/dev/null
      rm -f "${SAS_SSHAUTHSOCK_ORIG_SESS_LINK}"
    else
      screen -S "${SAS_SNAME}" -X unsetenv SSH_AUTH_SOCK  2>/dev/null
    fi
  else  # Session does not exist, or was just exited, so it's safe to cleanup
    rm -rf "${SAS_STATE}"
  fi

  rmdir "${SAS_STATE}" &>/dev/null;  rmdir "${SAS_STATE_ROOT}" &>/dev/null  # sure cleanup
}


# "sascreen" stands for ssh-agent screen wrapper
function sascreen {
  local SAS_ARGS=( "$@" )
  if [[ ${#SAS_ARGS[@]} -lt 2 ]]; then
    echo "sascreen: at least two args needed: reattach flag (e.g. -r or -R) and session name";  return 91
  fi

  local SAS_ARG_SNAME="${SAS_ARGS[-1]}"
  local SAS_ARGS_REMSIZE=$(( ${#SAS_ARGS[@]} - 1 ))
  local SAS_ARGS_REMAIN="${SAS_ARGS[@]:0:${SAS_ARGS_REMSIZE}}"

  __sas_up "${SAS_ARG_SNAME}";  screen ${SAS_ARGS_REMAIN} "${SAS_ARG_SNAME}";  __sas_down "${SAS_ARG_SNAME}"
}
